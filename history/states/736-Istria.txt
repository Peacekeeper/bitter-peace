
state = {
	id=736
	name="STATE_736" #Capodistria

	history = {
		victory_points = {
			599 1
		}
		owner = ITA
		add_core_of = ITA
		add_claim_by = YUG
		add_core_of = SLV
		buildings = {
			infrastructure = 2
		}
	}

	provinces={
		599 11595 
	}
	manpower=363159
	buildings_max_level_factor=1.000
	state_category=rural
	local_supplies=0.0
}
