state={
	id=842
	name="STATE_842"

	history={
		owner = YUG
		victory_points = {
			596 1
		}
		buildings = {
			infrastructure = 3
		}
		add_core_of = YUG
		add_core_of = SLV
	}

	provinces={
		596 665 3654 
	}
	manpower=134300
	buildings_max_level_factor=1.000
	state_category=town
	local_supplies=0.0
}
