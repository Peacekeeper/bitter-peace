
state={
	id=23
	name="STATE_23"
	resources={
		steel=30.000
	}

	history={
		owner = FRA
		buildings = {
			infrastructure = 3
			arms_factory = 1
		}
		add_core_of = FRA
		add_core_of = OCC
	}

	provinces={
		9597 595 11565 
	}
	manpower=520221
	buildings_max_level_factor=1.000
	state_category=town
	local_supplies=0.0 
}
