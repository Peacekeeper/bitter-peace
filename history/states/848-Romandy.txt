
state={
	id=848
	name="STATE_848"

	history={
		owner = SWI
		victory_points = {
			13317 1
		}
		victory_points = {
			13329 20
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 2
			9622 = {
				bunker = 2
			}
			6666 = {
				bunker = 2
			}
			11590 = {
				bunker = 4
			}
		}
		add_core_of = SWI
	}

	provinces={
		6666 6683 9622 13124 11590 13329
	}
	manpower=564222
	buildings_max_level_factor=1.000
	state_category=town
	local_supplies=0.0
}
