
state={
	id=869
	name="STATE_869"
	manpower=120658
	
	state_category = large_city

	history={
		owner = BEL
		add_core_of = BEL
		buildings = {
			infrastructure = 3
			industrial_complex = 1
		}
	}
	provinces={
		3488
	}
	
	buildings_max_level_factor=1
	state_category="city"
	local_supplies=0.0
}
