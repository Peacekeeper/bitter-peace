state={
	id=890
	name="STATE_890"
	history= {
		owner = ENG
		victory_points = {
			13301 1
		}
		buildings = {
			infrastructure = 2
			anti_air_building = 1
			air_base = 1
			13301 = {
				naval_base = 2
			}
		}
		add_core_of = ENG
	}
	provinces={
		13301 13313
	}
	manpower=3455
	buildings_max_level_factor=1.000
	state_category = small_island
	local_supplies=0.0
}
