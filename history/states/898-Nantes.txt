state={
	id=898
	name="STATE_898"

	history={
		owner = FRA
		victory_points = {
			11465 20
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			dockyard = 2
		}
		add_core_of = FRA

	}
	provinces={
		11616 11465 527
	}
	manpower=1659428
	buildings_max_level_factor=1.000
	state_category=city
	local_supplies=0.0
}
