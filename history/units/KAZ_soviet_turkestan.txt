﻿division_template = {
	name = "Strelkovaya Diviziya"			# Rifle Division
	division_names_group = SOV_INF_01

		regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
}

division_template = {
	name = "Kavaleriyskaya Diviziya" 	# Cavalry Division
	division_names_group = SOV_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
}

units = {	
	division= {	
		name = "1ya Strelkovaya Diviziya"
		location = 11537
		division_template = "Strelkovaya Diviziya"
		start_experience_factor = 0.0
		start_equipment_factor = 0.6
	}
	division= {	
		name = "2ya Strelkovaya Diviziya"
		location = 11537
		division_template = "Strelkovaya Diviziya"
		start_experience_factor = 0.0
		start_equipment_factor = 0.6
	}
	division= {	
		name = "3ya Strelkovaya Diviziya"
		location = 11537
		division_template = "Strelkovaya Diviziya"
		start_experience_factor = 0.0
		start_equipment_factor = 0.6
	}
	division= {	
		name = "4ya Strelkovaya Diviziya"
		location = 11537
		division_template = "Strelkovaya Diviziya"
		start_experience_factor = 0.0
		start_equipment_factor = 0.6
	}
	division= {	
		name = "5ya Strelkovaya Diviziya"
		location = 11537
		division_template = "Strelkovaya Diviziya"
		start_experience_factor = 0.0
		start_equipment_factor = 0.6
	}
	division= {
		name = "1ya Kavaleriyskaya Diviziya"
		location = 11537
		division_template = "Kavaleriyskaya Diviziya"
		start_experience_factor = 0.0
		start_equipment_factor = 0.6
	}	
	division= {
		name = "2ya Kavaleriyskaya Diviziya"
		location = 11537
		division_template = "Kavaleriyskaya Diviziya"
		start_experience_factor = 0.0
		start_equipment_factor = 0.6
	}
	division= {
		name = "3ya Kavaleriyskaya Diviziya"
		location = 11537
		division_template = "Kavaleriyskaya Diviziya"
		start_experience_factor = 0.0
		start_equipment_factor = 0.6
	}
	division= {
		name = "4ya Kavaleriyskaya Diviziya"
		location = 11537
		division_template = "Kavaleriyskaya Diviziya"
		start_experience_factor = 0.0
		start_equipment_factor = 0.6
	}
	division= {
		name = "5ya Kavaleriyskaya Diviziya"
		location = 11537
		division_template = "Kavaleriyskaya Diviziya"
		start_experience_factor = 0.0
		start_equipment_factor = 0.6
	}
}