﻿capital = 605

set_stability = 0.5
set_war_support = 0.3

set_technology = {
	infantry_weapons = 1
}

recruit_character = SIC_bao_wenyue

set_politics = {
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	democratic = 0
	fascism = 20
	communism = 0
	neutrality = 80
}

create_country_leader = {
	name = "Liu Xiang"
	desc = "POLITICS_LIU_XIANG_DESC"
	picture = "gfx/leaders/SIC/Portrait_Sichuan_Liu_Xiang.dds"
	expire = "1938.7.25"
	ideology = despotism
	traits = {
		
	}
}

create_country_leader = {
	name = "Bao Wenyue"
	desc = ""
	picture = "gfx/leaders/JAP/Portrait_Bao_Wenyue.dds"
	expire = "1962.7.25"
	ideology = fascism_ideology
	traits = {
		
	}
}
