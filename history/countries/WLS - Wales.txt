﻿capital = 122

oob = "WLS_1936"

# Starting tech
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_recon = 1
	tech_support = 1
	tech_engineers = 1
	tech_military_police = 1
	tech_mountaineers = 1
	motorised_infantry = 1
	paratroopers = 1
	gw_artillery = 1


	early_fighter = 1
	fighter1 = 1
	early_bomber = 1
	strategic_bomber1 = 1
	naval_bomber1 = 1
	mass_assault = 1
	fleet_in_being = 1
}
if = {
	limit = {
		NOT = {
			has_dlc = "No Step Back"
		}
	}
	set_technology = {
		gwtank = 1
		basic_light_tank = 1
	}
}
if = {
	limit = {
		
		has_dlc = "No Step Back"
		
	}
	set_technology = {
		gwtank_chassis = 1
		basic_light_tank_chassis = 1
	}
}
if = {
	limit = {
		not = { has_dlc = "Man the Guns" }
	}
	set_technology = {
		early_submarine = 1
		basic_submarine = 1
		early_destroyer = 1
		early_light_cruiser = 1
		early_heavy_cruiser = 1
		early_battleship = 1
		early_battlecruiser = 1
		transport = 1
	}
}
if = {
	limit = {
		has_dlc = "Man the Guns"
	}
	set_technology = {
		early_ship_hull_submarine = 1
		basic_ship_hull_submarine = 1
		early_ship_hull_light = 1
		early_ship_hull_cruiser = 1
		early_ship_hull_heavy = 1
		mtg_transport = 1
		basic_battery = 1
		basic_torpedo = 1
	}
}

set_convoys = 20

recruit_character = WLS_roderick_richards
recruit_character = WLS_saunders_lewis
recruit_character = WLS_arthur_horner
recruit_character = WLS_gwynfor_evans

recruit_character = WLS_acr
recruit_character = WLS_stc
recruit_character = WLS_awt
recruit_character = WLS_pot
recruit_character = WLS_mt
recruit_character = WLS_ncm
recruit_character = WLS_nt
recruit_character = WLS_acd2
recruit_character = WLS_fascist_guy
recruit_character = WLS_ncs
recruit_character = WLS_nccr
recruit_character = WLS_aco2
recruit_character = WLS_communist_guy
recruit_character = WLS_ai2
recruit_character = WLS_ar
recruit_character = WLS_coi
recruit_character = WLS_acas
recruit_character = WLS_acgs
recruit_character = WLS_aa2
recruit_character = WLS_democratic_guy

1939.1.1 = {
	#generic focuses
	complete_national_focus = army_effort
	complete_national_focus = equipment_effort
	complete_national_focus = motorization_effort
	complete_national_focus = aviation_effort
	complete_national_focus = naval_effort
	complete_national_focus = flexible_navy
	complete_national_focus = industrial_effort
	complete_national_focus = construction_effort
	complete_national_focus = production_effort
	
	set_technology = {

		#doctrines
		grand_battle_plan = 1
		trench_warfare = 1

		#electronics
		electronic_mechanical_engineering = 1
		radio = 1
		radio_detection = 1
		mechanical_computing = 1

		#industry
		basic_machine_tools = 1
		improved_machine_tools = 1
		advanced_machine_tools = 1
		construction1 = 1
		construction2 = 1
		dispersed_industry = 1
		dispersed_industry2 = 1
	}
}
set_politics = {
	ruling_party = democratic
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	democratic = 93
	fascism = 3
	communism = 4
}

#create_country_leader = {
#	
#	name = "Jonas Lote"
#	picture = "gfx//leaders//Africa//Portrait_Africa_Generic_2.dds"
#	expire = "1965.1.1"
#	ideology = centrism
#	traits = {
#		#
#	}
#}
