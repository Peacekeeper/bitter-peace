guiTypes = {
	containerWindowType = { # Button script
		name = "BP_germanization_window_open"
		size = { width = 150 height = 141 }
		position = { x = 8 y = -180 }
		Orientation = UPPER_LEFT
		clipping = no

		iconType = {
			name = "continuous_glow"
			position = { x = 155 y = 12 }
			quadTextureSprite = "GFX_ongoing_generic_glow_yellow"
			Orientation = UPPER_LEFT
			alwaystransparent = yes
			scale = 0.95
		}

		buttonType = {
			name = "BP_germanization_menu_open"
			position = { x = 170 y = 25 }
			quadTextureSprite = "GFX_germanization_icon"
			clicksound = click_default
			Orientation = UPPER_LEFT
			pdx_tooltip = "GERMANIZATION_MENU_OPEN_DESC"
			scale = 0.60
		}
	}

	containerWindowType = {
		name = "BP_germanization_window_main"
		orientation = UPPER_LEFT	
		size = { width = 675 height = 600 }
		position = { x = 541 y = 79 }
		
		show_sound = menu_open_window
		click_to_front = yes
        
		background = {
			name = "Background"		
			quadTextureSprite = "GFX_tiled_window_thin_border2"
		}

		iconType = {
			name = "map_background_asset"
			spriteType = "GFX_germanization_map_bg"
			position = { x = 50 y = 70 }
			pdx_tooltip = ""
		}

		iconType = {
			name = "title_background_asset"
			spriteType = "GFX_germanization_title_bg"
			orientation = UPPER_LEFT
			position = { x = 14 y = 15 }
			pdx_tooltip = ""
		}

        instantTextboxType = {
			name = "title"
			position = { x = 220 y = 15 }
			font = "hoi_36header"
			borderSize = {x = 0 y = 0 }
			text = "GERMANIZATION_TITLE"
			maxWidth = 240
			maxHeight = 20
			format = CENTER	
		}

		buttonType = {
			name = "close_button"
			position = { x = -53 y = 18 }
			quadTextureSprite ="GFX_closebutton"
			buttonFont = "Main_14_black"
			shortcut = "ESCAPE"
			Orientation = "UPPER_RIGHT"
			clicksound = click_close
			pdx_tooltip = "CLOSE"
		}

		buttonType = {
			name = "BP_germanization_german_reich"
			spriteType = "GFX_german_reich"
			position = { x = 20 y = -70 }
			orientation = center
			centerposition = yes
			alwaystransparent = yes
		}

		buttonType = {
			name = "BP_germanization_austria"
			spriteType = "GFX_austria"
			position = { x = -85 y = -26 }
			orientation = center
			alwaystransparent = yes
		}

		buttonType = {
			name = "BP_germanization_BuM_button"
			spriteType = "GFX_BuM"
			position = { x = -32  y = -60 }
			orientation = center
		}

		buttonType = {
			name = "BP_germanization_sudetenland"
			spriteType = "GFX_sudetenland"
			position = { x = -44 y = -67 }
			orientation = center
			alwaystransparent = yes
		}

		buttonType = {
			name = "BP_germanization_memel"
			spriteType = "GFX_memel"
			position = { x = 94 y = -167 }
			orientation = center
			alwaystransparent = yes
		}

		buttonType = {
			name = "BP_germanization_west_prussia_button"
			spriteType = "GFX_west_prussia"
			position = { x = 39 y = -146 }
			orientation = center
		}

		buttonType = {
			name = "BP_germanization_suwalken_button"
			spriteType = "GFX_suwalken"
			position = { x = 120 y = -137 }
			orientation = center
		}

		buttonType = {
			name = "BP_germanization_west_poland_button"
			spriteType = "GFX_west_poland"
			position = { x = 15 y = -117 }
			orientation = center
		}

		buttonType = {
			name = "BP_germanization_east_upper_silesia_button"
			spriteType = "GFX_east_upper_silesia"
			position = { x = 54 y = -66 }
			orientation = center
		}

		buttonType = {
			name = "BP_germanization_pgg_button"
			spriteType = "GFX_pgg"
			position = { x = 65 y = -105 }
			orientation = center
		}

		buttonType = {
			name = "BP_germanization_galicia_button"
			spriteType = "GFX_galicia"
			position = { x = 115 y = -57 }
			orientation = center
		}

		buttonType = {
			name = "BP_germanization_north_schleswig_button"
			spriteType = "GFX_north_schleswig"
			position = { x = -100 y = -160 }
			orientation = center
		}

		buttonType = {
			name = "BP_germanization_eupen_malmedy_button"
			spriteType = "GFX_eupen_malmedy"
			position = { x = -142 y = -61 }
			orientation = center
		}

		buttonType = {
			name = "BP_germanization_luxembourg_button"
			spriteType = "GFX_luxembourg"
			position = { x = -142 y = -49 }
			orientation = center
		}

		buttonType = {
			name = "BP_germanization_elsass_lothringen_button"
			spriteType = "GFX_elsass_lothringen"
			position = { x = -142 y = -36 }
			orientation = center
		}
	}

	### West Prussia window
	containerWindowType = {
		name = "BP_germanization_west_prussia_info"
		size = {width = 600 height = 300}
		position = { x = 0 y = 0 }
		Orientation = UPPER_LEFT
		margin = {top = 9 bottom = 9}
		moveable = no

		instantTextBoxType = {
			name = "name_of_region"
			position = { x = 570 y = 470 }
			font = "hoi_30header"
			text = "WEST_PRUSSIA_TITLE"
			Orientation = UPPER_LEFT
			format = left
			pdx_tooltip = "NAME_OF_REGION_DESC"
			maxHeight = 500
			maxWidth = 500
		}

		instantTextBoxType = {
			name = "germanization_progress"
			position = { x = 1000 y = 500 }
			font = "hoi_20b"
			text = "GERMANIZATION_PROGRESS"
			pdx_tooltip = "GERMANIZATION_DESC"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "nazification_progress"
			position = { x = 1000 y = 530 }
			font = "hoi_20b"
			text = "NAZIFICATION_PROGRESS"
			pdx_tooltip = "NAZIFICATION_DESC"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		buttonType = {
			name = "inspear_germanization1"
			position = { x = 940 y = 560 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_germanization2"
			position = { x = 940 y = 610 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_nazification1"
			position = { x = 1070 y = 610 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_nazification2"
			position = { x = 1070 y = 560 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		instantTextBoxType = {
			name = "state_danzig"
			position = { x = 570 y = 500 }
			font = "hoi_24header"
			text = "[85.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_danzig_population"
			position = { x = 570 y = 520 }
			font = "hoi_20b"
			text = "STATE_85_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_danzig_resistance"
			position = { x = 570 y = 540 }
			font = "hoi_20b"
			text = "STATE_85_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_west_prussia"
			position = { x = 770 y = 500 }
			font = "hoi_24header"
			text = "[892.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_west_prussia_population"
			position = { x = 770 y = 520 }
			font = "hoi_20b"
			text = "STATE_892_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_west_prussia_resistance"
			position = { x = 770 y = 540 }
			font = "hoi_20b"
			text = "STATE_892_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}
	}	

	### Suwalki window
	containerWindowType = {
		name = "BP_germanization_suwalken_info"
		size = { width = 600 height = 300}
		position = { x = 0 y = 0 }
		Orientation = UPPER_LEFT
		margin = {top = 9 bottom = 9}
		moveable = no

		instantTextBoxType = {
			name = "name_of_region"
			position = { x = 570 y = 470 }
			font = "hoi_30header"
			text = "SUWALKEN_TITLE"
			Orientation = UPPER_LEFT
			format = left
			pdx_tooltip = "NAME_OF_REGION_DESC"
			maxHeight = 500
			maxWidth = 500
		}

		instantTextBoxType = {
			name = "germanization_progress"
			position = { x = 1000 y = 500 }
			font = "hoi_20b"
			text = "GERMANIZATION_PROGRESS"
			pdx_tooltip = "GERMANIZATION_DESC"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "nazification_progress"
			position = { x = 1000 y = 530 }
			font = "hoi_20b"
			text = "NAZIFICATION_PROGRESS"
			pdx_tooltip = "NAZIFICATION_DESC"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		buttonType = {
			name = "inspear_germanization1"
			position = { x = 940 y = 560 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_germanization2"
			position = { x = 940 y = 610 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_nazification1"
			position = { x = 1070 y = 610 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_nazification2"
			position = { x = 1070 y = 560 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		instantTextBoxType = {
			name = "state_suwalken"
			position = { x = 570 y = 500 }
			font = "hoi_24header"
			text = "[887.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_suwalken_population"
			position = { x = 570 y = 520 }
			font = "hoi_20b"
			text = "STATE_899_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_suwalken_resistance"
			position = { x = 570 y = 540 }
			font = "hoi_20b"
			text = "STATE_899_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}
	}

	### West Poland window
	containerWindowType = {
		name = "BP_germanization_west_poland_info"
		size = { width = 600 height = 300}
		position = { x = 0 y = 0 }
		Orientation = UPPER_LEFT
		margin = {top = 9 bottom = 9}
		moveable = no

		instantTextBoxType = {
			name = "name_of_region"
			position = { x = 570 y = 470 }
			font = "hoi_30header"
			text = "WARTHELAND_TITLE"
			Orientation = UPPER_LEFT
			format = left
			pdx_tooltip = "NAME_OF_REGION_DESC"
			maxHeight = 500
			maxWidth = 500
		}

		instantTextBoxType = {
			name = "germanization_progress"
			position = { x = 1000 y = 500 }
			font = "hoi_20b"
			text = "GERMANIZATION_PROGRESS"
			pdx_tooltip = "GERMANIZATION_DESC"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "nazification_progress"
			position = { x = 1000 y = 530 }
			font = "hoi_20b"
			text = "NAZIFICATION_PROGRESS"
			pdx_tooltip = "NAZIFICATION_DESC"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		buttonType = {
			name = "inspear_germanization1"
			position = { x = 940 y = 560 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_germanization2"
			position = { x = 940 y = 610 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_nazification1"
			position = { x = 1070 y = 610 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_nazification2"
			position = { x = 1070 y = 560 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		instantTextBoxType = {
			name = "state_poznan"
			position = { x = 570 y = 500 }
			font = "hoi_24header"
			text = "[86.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_poznan_population"
			position = { x = 570 y = 520 }
			font = "hoi_20b"
			text = "STATE_86_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_poznan_resistance"
			position = { x = 570 y = 540 }
			font = "hoi_20b"
			text = "STATE_86_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_zichenau"
			position = { x = 570 y = 570 }
			font = "hoi_24header"
			text = "[98.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_zichenau_population"
			position = { x = 570 y = 590 }
			font = "hoi_20b"
			text = "STATE_98_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_zichenau_resistance"
			position = { x = 570 y = 610 }
			font = "hoi_20b"
			text = "STATE_98_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_wartheland"
			position = { x = 770 y = 500 }
			font = "hoi_24header"
			text = "[87.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_wartheland_population"
			position = { x = 770 y = 520 }
			font = "hoi_20b"
			text = "STATE_87_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_wartheland_resistance"
			position = { x = 770 y = 540 }
			font = "hoi_20b"
			text = "STATE_87_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}
	}

	### East Upper Silesia window
	containerWindowType = {
		name = "BP_germanization_east_upper_silesia_info"
		size = { width = 600 height = 300}
		position = { x = 0 y = 0 }
		Orientation = UPPER_LEFT
		margin = {top = 9 bottom = 9}
		moveable = no

		instantTextBoxType = {
			name = "name_of_region"
			position = { x = 570 y = 470 }
			font = "hoi_30header"
			text = "EAST_UPPER_SILESIA_TITLE"
			Orientation = UPPER_LEFT
			format = left
			pdx_tooltip = "NAME_OF_REGION_DESC"
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "germanization_progress"
			position = { x = 1000 y = 500 }
			font = "hoi_20b"
			text = "GERMANIZATION_PROGRESS"
			pdx_tooltip = "GERMANIZATION_DESC"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "nazification_progress"
			position = { x = 1000 y = 530 }
			font = "hoi_20b"
			text = "NAZIFICATION_PROGRESS"
			pdx_tooltip = "NAZIFICATION_DESC"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		buttonType = {
			name = "inspear_germanization1"
			position = { x = 940 y = 560 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_germanization2"
			position = { x = 940 y = 610 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_nazification1"
			position = { x = 1070 y = 610 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_nazification2"
			position = { x = 1070 y = 560 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		instantTextBoxType = {
			name = "state_kattowitz"
			position = { x = 570 y = 500 }
			font = "hoi_24header"
			text = "[762.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_kattowitz_population"
			position = { x = 570 y = 520 }
			font = "hoi_20b"
			text = "STATE_762_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_kattowitz_resistance"
			position = { x = 570 y = 540 }
			font = "hoi_20b"
			text = "STATE_762_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_auscwitz"
			position = { x = 570 y = 570 }
			font = "hoi_24header"
			text = "[885.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_auscwitz_population"
			position = { x = 570 y = 590 }
			font = "hoi_20b"
			text = "STATE_897_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_auscwitz_resistance"
			position = { x = 570 y = 610 }
			font = "hoi_20b"
			text = "STATE_897_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_olsaland"
			position = { x = 770 y = 500 }
			font = "hoi_24header"
			text = "[72.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_olsaland_population"
			position = { x = 770 y = 520 }
			font = "hoi_20b"
			text = "STATE_72_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_olsaland_resistance"
			position = { x = 770 y = 540 }
			font = "hoi_20b"
			text = "STATE_72_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}
	}

	### Generalgouvernement window
	containerWindowType = {
		name = "BP_germanization_pgg_info"
		size = { width = 600 height = 300}
		position = { x = 0 y = 0 }
		Orientation = UPPER_LEFT
		margin = {top = 9 bottom = 9}
		moveable = no

		instantTextBoxType = {
			name = "name_of_region"
			position = { x = 570 y = 470 }
			font = "hoi_30header"
			text = "PGG_TITLE"
			Orientation = UPPER_LEFT
			format = left
			pdx_tooltip = "NAME_OF_REGION_DESC"
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "germanization_progress"
			position = { x = 1000 y = 500 }
			font = "hoi_20b"
			text = "GERMANIZATION_PROGRESS"
			pdx_tooltip = "GERMANIZATION_DESC"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "nazification_progress"
			position = { x = 1000 y = 530 }
			font = "hoi_20b"
			text = "NAZIFICATION_PROGRESS"
			pdx_tooltip = "NAZIFICATION_DESC"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		buttonType = {
			name = "inspear_germanization1"
			position = { x = 940 y = 560 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_germanization2"
			position = { x = 940 y = 610 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_nazification1"
			position = { x = 1070 y = 610 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_nazification2"
			position = { x = 1070 y = 560 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		instantTextBoxType = {
			name = "state_warschau"
			position = { x = 570 y = 500 }
			font = "hoi_24header"
			text = "[10.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_warschau_population"
			position = { x = 570 y = 520 }
			font = "hoi_20b"
			text = "STATE_10_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_warschau_resistance"
			position = { x = 570 y = 540 }
			font = "hoi_20b"
			text = "STATE_10_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_vandalenland"
			position = { x = 570 y = 570 }
			font = "hoi_24header"
			text = "[92.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_vandalenland_population"
			position = { x = 570 y = 590 }
			font = "hoi_20b"
			text = "STATE_92_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_vandalenland_resistance"
			position = { x = 570 y = 610 }
			font = "hoi_20b"
			text = "STATE_92_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_weichselland"
			position = { x = 770 y = 500 }
			font = "hoi_24header"
			text = "[90.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_weichselland_population"
			position = { x = 770 y = 520 }
			font = "hoi_20b"
			text = "STATE_90_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_weichselland_resistance"
			position = { x = 770 y = 540 }
			font = "hoi_20b"
			text = "STATE_90_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_krakau"
			position = { x = 770 y = 570 }
			font = "hoi_24header"
			text = "[88.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_krakau_population"
			position = { x = 770 y = 590 }
			font = "hoi_20b"
			text = "STATE_88_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_krakau_resistance"
			position = { x = 770 y = 610 }
			font = "hoi_20b"
			text = "STATE_88_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}
	}

	### Distrikt Galizien window
	containerWindowType = {
		name = "BP_germanization_galizia_info"
		size = { width = 600 height = 300}
		position = { x = 0 y = 0 }
		Orientation = UPPER_LEFT
		margin = {top = 9 bottom = 9}
		moveable = no

		instantTextBoxType = {
			name = "name_of_region"
			position = { x = 570 y = 470 }
			font = "hoi_30header"
			text = "GALIZIA_TITLE"
			Orientation = UPPER_LEFT
			format = left
			pdx_tooltip = "NAME_OF_REGION_DESC"
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "germanization_progress"
			position = { x = 1000 y = 500 }
			font = "hoi_20b"
			text = "GERMANIZATION_PROGRESS"
			pdx_tooltip = "GERMANIZATION_DESC"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "nazification_progress"
			position = { x = 1000 y = 530 }
			font = "hoi_20b"
			text = "NAZIFICATION_PROGRESS"
			pdx_tooltip = "NAZIFICATION_DESC"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		buttonType = {
			name = "inspear_germanization1"
			position = { x = 940 y = 560 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_germanization2"
			position = { x = 940 y = 610 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_nazification1"
			position = { x = 1070 y = 610 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		buttonType = {
			name = "inspear_nazification2"
			position = { x = 1070 y = 560 }
			quadTextureSprite = "GFX_germanization_button"
			buttonFont = "hoi_20b"
			Orientation = UPPER_LEFT
		}

		instantTextBoxType = {
			name = "state_premissel"
			position = { x = 570 y = 500 }
			font = "hoi_24header"
			text = "[901.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_premissel_population"
			position = { x = 570 y = 520 }
			font = "hoi_20b"
			text = "STATE_898_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_premissel_resistance"
			position = { x = 570 y = 540 }
			font = "hoi_20b"
			text = "STATE_898_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_lemberg"
			position = { x = 570 y = 570 }
			font = "hoi_24header"
			text = "[91.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_lemberg_population"
			position = { x = 570 y = 590 }
			font = "hoi_20b"
			text = "STATE_91_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_lemberg_resistance"
			position = { x = 570 y = 610 }
			font = "hoi_20b"
			text = "STATE_91_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_stanislau"
			position = { x = 770 y = 500 }
			font = "hoi_24header"
			text = "[89.GetName]"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_stanislau_population"
			position = { x = 770 y = 520 }
			font = "hoi_20b"
			text = "STATE_89_POPULATION"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}

		instantTextBoxType = {
			name = "state_stanislau_resistance"
			position = { x = 770 y = 540 }
			font = "hoi_20b"
			text = "STATE_89_RESISTANCE"
			Orientation = UPPER_LEFT
			format = left
			maxHeight = 24
			maxWidth = 320
		}
	}
}