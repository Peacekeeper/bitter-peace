autonomy_state = {
	id = autonomy_wtt_imperial_subject #Eisei Kuni, aka Satellite State, these would be the numerous puppet kingdoms such as Empire of Vietnam and the East Indies Kingdom that Japan was attempting to set up. In-game Satellite or "Dominion level puppet.
	
	is_puppet = no
	min_freedom_level = 0.6
	manpower_influence = 0.4
	
	rule = {
		desc = "RULE_DESC_IS_A_SUBJECT"

		can_not_declare_war = yes
		can_decline_call_to_war = yes
		units_deployed_to_overlord = no
		can_be_spymaster = yes
		contributes_operatives = yes
		can_create_collaboration_government = yes
	}
	
	modifier = {
		autonomy_manpower_share = 0.4
		extra_trade_to_overlord_factor = 0.4
		overlord_trade_cost_factor = -0.4
		license_subject_master_purchase_cost = -0.5
		autonomy_gain_global_factor = -0.1
	}
	
	ai_subject_wants_higher = {
		factor = 1.0
	}
	
	ai_overlord_wants_lower = {
		factor = 1.0
	}

	ai_overlord_wants_garrison = {
		always = no
	}

	allowed = {
		has_dlc = "Together for Victory"
		OVERLORD = {
			OR = {
				has_government = fascism
				has_government = neutrality
			}
			OR = {
				original_tag = JAP
				original_tag = MAN
			}
		}
	}
	
	can_take_level = {
		#always = no
	}

	can_lose_level = {
		#trigger here
	}
}