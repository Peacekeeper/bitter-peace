autonomy_state = {
	id = autonomy_nebenland

	is_puppet = yes
	use_overlord_color = yes
	min_freedom_level = 0.0
	manpower_influence = 0.8
	
	rule = {
		desc = "RULE_DESC_IS_A_SUBJECT"

		can_not_declare_war = yes
		can_decline_call_to_war = no
		units_deployed_to_overlord = yes
		can_be_spymaster = no
		contributes_operatives = no
		can_create_collaboration_government = no
	}
	
	modifier = {
		autonomy_manpower_share = 0.8
		can_master_build_for_us = 1
		conscription_factor = -0.25
		extra_trade_to_overlord_factor = 0.8
		overlord_trade_cost_factor = -0.9
		cic_to_overlord_factor = 0.25
		mic_to_overlord_factor = 0.50
		research_sharing_per_country_bonus_factor = -0.3
		license_subject_master_purchase_cost = -1
		autonomy_gain_global_factor = -0.5
	}
	
	ai_subject_wants_higher = {
		factor = 1.0
	}
	
	ai_overlord_wants_lower = {
		factor = 0.0
	}

	ai_overlord_wants_garrison = {
		always = yes
	}

	allowed = {
		has_dlc = "Together for Victory"
		OVERLORD = { 
			original_tag = GER
			has_government = fascism
		}
		NOT = {
			has_country_flag = bp_reichskommissariat
			capital_scope = {
				OR = {
					is_on_continent = africa
					is_on_continent = asia
				}
			}
			tag = BRG
			has_cosmetic_tag = BRI_ordensstaat
		}
	}

	can_take_level = {
		#trigger here
	}

	can_lose_level = {
		#trigger here
	}
}