#	Example:
# 
#	example_effect = {
#		add_political_power = 66
#		add_popularity = {
#			ideology = fascism
#			popularity = 0.33
#		}
#	}
#
#
#	In a script file:
#
#	effect = {
#		example_effect = yes
#	}
#

SS_set_templates = {
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_infantry_battalion
			}
		}
		SS_infantry_battalion = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_cavalry_regiment
			}
		}
		SS_cavalry_regiment = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_infantry_regiment
			}
		}
		SS_infantry_regiment = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_motorised_regiment_early
			}
		}
		SS_motorised_regiment_early = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_infantry_division_late
			}
		}
		SS_infantry_division_late = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_mountain_battalion
			}
		}
		SS_mountain_battalion = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_mountain_division_early
			}
		}
		SS_mountain_division_early = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_mountain_division_late
			}
		}
		SS_mountain_division_late = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_cavalry_division_early
			}
		}
		SS_cavalry_division_early = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_cavalry_division_late
			}
		}
		SS_cavalry_division_late = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_motorised_division_early
			}
		}
		SS_motorised_division_early = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_motorised_division_late
			}
		}
		SS_motorised_division_late = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_mechanised_division_early
			}
		}
		SS_mechanised_division_early = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_mechanised_division_late
			}
		}
		SS_mechanised_division_late = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_armored_division_early
			}
		}
		SS_armored_division_early = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_armored_division_late
			}
		}
		SS_armored_division_late = yes
	}
}

SS_set_mechanised_templates = {
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_mechanised_division_early
			}
			has_tech = mechanised_infantry
			NOT = {
				AND = {
					OR = {
						has_tech = basic_medium_tank
						has_tech = improved_medium_tank
						has_tech = advanced_medium_tank
					}
					OR = {
						has_tech = basic_medium_td
						has_tech = improved_medium_td
						has_tech = advanced_medium_td
					}
					OR = {
						has_tech = basic_medium_art
						has_tech = improved_medium_art
						has_tech = advanced_medium_art
					}
				}
			}
		}
		SS_mechanised_division_early = yes
	}
	if = {
		limit = {
			NOT = {
				has_country_flag = SS_mechanised_division_late
			}
			has_tech = mechanised_infantry
			OR = {
				has_tech = basic_medium_tank
				has_tech = improved_medium_tank
				has_tech = advanced_medium_tank
			}
			OR = {
				has_tech = basic_medium_td
				has_tech = improved_medium_td
				has_tech = advanced_medium_td
			}
			OR = {
				has_tech = basic_medium_art
				has_tech = improved_medium_art
				has_tech = advanced_medium_art
			}
		}
		SS_mechanised_division_late = yes
	}
}
SS_infantry_battalion = {
	set_country_flag = SS_infantry_battalion
	division_template = {
		name = "SS-Battalion"
		priority = 2
		is_locked = yes
		regiments = {
			SS_infantry = { x = 0 y = 0 }
		}
		support = {

		}
	}
}
SS_infantry_regiment = {
	set_country_flag = SS_infantry_regiment
	division_template = {
		name = "SS-Regiment"
		priority = 2
		is_locked = yes
		regiments = {
			SS_infantry = { x = 0 y = 0 }
			SS_infantry = { x = 0 y = 1 }
			SS_infantry = { x = 0 y = 2 }
		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			signal_company = { x = 0 y = 2 }
			military_police = { x = 0 y = 3 }
			anti_air = { x = 0 y = 4 }
		}
	}
}
SS_cavalry_regiment = {
	set_country_flag = SS_cavalry_regiment
	division_template = {
		name = "SS-Kav. Regiment"
		priority = 2
		is_locked = yes
		regiments = {
			SS_cavalry = { x = 0 y = 0 }
			SS_cavalry = { x = 0 y = 1 }
			SS_cavalry = { x = 0 y = 2 }
		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			signal_company = { x = 0 y = 2 }
			military_police = { x = 0 y = 3 }
			anti_air = { x = 0 y = 4 }
		}
	}
}
SS_motorised_regiment_early = { #For 1. SS-Division 'Leibstandarte SS Adolf Hitler'
	set_country_flag = SS_motorised_regiment_early
	division_template = {
		name = "SS-Regiment (mot.)"
		priority = 2
		is_locked = yes
		regiments = {
			SS_motorized = { x = 0 y = 0 }
			SS_motorized = { x = 0 y = 1 }
			SS_motorized = { x = 0 y = 2 }
		}
		support = {
			engineer = { x = 0 y = 0 }
			artillery = { x = 0 y = 1 }
		}
	}
}
SS_motorised_regiment_late = {
	set_country_flag = SS_motorised_regiment_late
	division_template = {
		name = "SS-Regiment (mot.)"
		priority = 2
		is_locked = yes
		regiments = {
			SS_motorized = { x = 0 y = 0 }
			SS_motorized = { x = 0 y = 1 }
			SS_motorized = { x = 0 y = 2 }
		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			signal_company = { x = 0 y = 2 }
			military_police = { x = 0 y = 3 }
			anti_air = { x = 0 y = 4 }
		}
	}
}
SS_infantry_brigade_early = {
	set_country_flag = SS_infantry_brigade_early
	division_template = {
		name = "SS-Brigade"
		priority = 2
		is_locked = yes
		regiments = {
			SS_infantry = { x = 0 y = 0 }
			SS_infantry = { x = 0 y = 1 }
			SS_infantry = { x = 0 y = 2 }

			SS_infantry = { x = 1 y = 0 }
			SS_infantry = { x = 1 y = 1 }
			SS_infantry = { x = 1 y = 2 }
		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			anti_air = { x = 0 y = 4 }
		}
	}
}
SS_infantry_brigade_late = { #For 20. SS-Division 'Estniche Nr. 1', 27. SS-Division 'Langemarck' and 28. SS-Division 'Wallonien' and late units
	set_country_flag = SS_infantry_brigade_late
	division_template = {
		name = "SS-Brigade (n.A.)"
		priority = 2
		is_locked = yes
		regiments = {
			SS_infantry = { x = 0 y = 0 }
			SS_infantry = { x = 0 y = 1 }
			SS_infantry = { x = 0 y = 2 }

			SS_infantry = { x = 1 y = 0 }
			SS_infantry = { x = 1 y = 1 }
			SS_infantry = { x = 1 y = 2 }

			SS_mot_artillery_brigade = { x = 2 y = 0 }
			SS_mot_anti_tank_brigade = { x = 2 y = 1 }
		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			signal_company = { x = 0 y = 2 }
			military_police = { x = 0 y = 3 }
			anti_air = { x = 0 y = 4 }
		}
	}
}
SS_cavalry_brigade = {
	set_country_flag = SS_cavalry_brigade
	division_template = {
		name = "SS-Kav. Brigade"
		priority = 2
		is_locked = yes
		regiments = {
			SS_cavalry = { x = 0 y = 0 }
			SS_cavalry = { x = 0 y = 1 }
			SS_cavalry = { x = 0 y = 2 }

			SS_cavalry = { x = 1 y = 0 }
			SS_cavalry = { x = 1 y = 1 }
			SS_cavalry = { x = 1 y = 2 }

			SS_mot_artillery_brigade = { x = 2 y = 0 }
			SS_mot_anti_tank_brigade = { x = 2 y = 1 }
		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			signal_company = { x = 0 y = 2 }
			military_police = { x = 0 y = 3 }
			anti_air = { x = 0 y = 4 }
		}
	}
}
SS_motorised_brigade = {
	set_country_flag = SS_motorised_brigade
	division_template = {
		name = "SS-Brigade (mot.)"
		priority = 2
		is_locked = yes
		regiments = {
			SS_motorized = { x = 0 y = 0 }
			SS_motorized = { x = 0 y = 1 }
			SS_motorized = { x = 0 y = 2 }

			SS_motorized = { x = 1 y = 0 }
			SS_motorized = { x = 1 y = 1 }
			SS_motorized = { x = 1 y = 2 }
		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			artillery = { x = 0 y = 2 }
		}
	}
}
SS_mechanised_brigade = {
	set_country_flag = SS_mechanised_brigade
	division_template = {
		name = "SS-Pz.Gren. Brigade"
		priority = 2
		is_locked = yes
		regiments = {
			SS_mechanized = { x = 0 y = 0 }
			SS_mechanized = { x = 0 y = 1 }
			SS_mechanized = { x = 0 y = 2 }

			SS_mechanized = { x = 1 y = 0 }
			SS_mechanized = { x = 1 y = 1 }
			SS_mechanized = { x = 1 y = 2 }

			SS_mot_artillery_brigade = { x = 2 y = 0 }
			SS_mot_anti_tank_brigade = { x = 2 y = 1 }
		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			signal_company = { x = 0 y = 2 }
			military_police = { x = 0 y = 3 }
			anti_air = { x = 0 y = 4 }
		}
	}
}
SS_infantry_division_early = {
	set_country_flag = SS_infantry_division_early
	division_template = {
		name = "SS-Gren. Div."
		priority = 2
		division_names_group = SS_INF_01
		is_locked = yes
		regiments = {
			SS_infantry = { x = 0 y = 0 }
			SS_infantry = { x = 0 y = 1 }
			SS_infantry = { x = 0 y = 2 }

			SS_infantry = { x = 1 y = 0 }
			SS_infantry = { x = 1 y = 1 }
			SS_infantry = { x = 1 y = 2 }

			SS_infantry = { x = 2 y = 0 }
			SS_infantry = { x = 2 y = 1 }
			SS_infantry = { x = 2 y = 2 }
		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			artillery = { x = 0 y = 4 }
		}
	}
}
SS_infantry_division_late = {
	set_country_flag = SS_infantry_division_late
	division_template = {
		name = "SS-Gren. Div. (n.A.)"
		priority = 2
		division_names_group = SS_INF_01
		is_locked = yes
		regiments = {
			SS_infantry = { x = 0 y = 0 }
			SS_infantry = { x = 0 y = 1 }
			SS_infantry = { x = 0 y = 2 }

			SS_infantry = { x = 1 y = 0 }
			SS_infantry = { x = 1 y = 1 }
			SS_infantry = { x = 1 y = 2 }

			SS_infantry = { x = 2 y = 0 }
			SS_infantry = { x = 2 y = 1 }
			SS_infantry = { x = 2 y = 2 }

			SS_artillery_brigade = { x = 3 y = 0 }
			SS_anti_air_brigade = { x = 3 y = 1 }
			SS_anti_tank_brigade = { x = 3 y = 2 }
			SS_anti_tank_brigade = { x = 3 y = 3 }
		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			field_hospital = { x = 0 y = 2 }
			signal_company = { x = 0 y = 3 }
			artillery = { x = 0 y = 4 }
		}
	}
}
SS_mountain_battalion = {
	set_country_flag = SS_mountain_battalion
	division_template = {
		name = "SS-Gebirgs Battalion"
		priority = 2
		division_names_group = SS_MNT_01
		is_locked = yes
		regiments = {
			SS_mountaineers = { x = 0 y = 0 }
		}
		support = {

		}
	}
}
SS_mountain_division_early = {
	set_country_flag = SS_mountain_division_early
	division_template = {
		name = "SS-Gebirgs Div."
		priority = 2
		division_names_group = SS_MNT_01
		is_locked = yes
		regiments = {
			SS_mountaineers = { x = 0 y = 0 }
			SS_mountaineers = { x = 0 y = 1 }
			SS_mountaineers = { x = 0 y = 2 }
			
			SS_mountaineers = { x = 1 y = 0 }
			SS_mountaineers = { x = 1 y = 1 }
			SS_mountaineers = { x = 1 y = 2 }

			SS_mountaineers = { x = 2 y = 0 }
			SS_mountaineers = { x = 2 y = 1 }
			SS_mountaineers = { x = 2 y = 2 }

		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			rocket_artillery = { x = 0 y = 2 }
		}
	}
}
SS_mountain_division_late = {
	set_country_flag = SS_mountain_division_late
	division_template = {
		name = "SS-Gebirgs Div. (n.A.)"
		priority = 2
		division_names_group = SS_MNT_01
		is_locked = yes
		regiments = {
			SS_mountaineers = { x = 0 y = 0 }
			SS_mountaineers = { x = 0 y = 1 }
			SS_mountaineers = { x = 0 y = 2 }
			
			SS_mountaineers = { x = 1 y = 0 }
			SS_mountaineers = { x = 1 y = 1 }
			SS_mountaineers = { x = 1 y = 2 }
			
			SS_mountaineers = { x = 2 y = 0 }
			SS_artillery_brigade = { x = 2 y = 1 }
			SS_anti_tank_brigade = { x = 2 y = 2 }
			SS_anti_air_brigade = { x = 2 y = 3 }

		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			field_hospital = { x = 0 y = 2 }
			signal_company = { x = 0 y = 3 }
			rocket_artillery = { x = 0 y = 4 }
		}
	}
}
SS_cavalry_division_early = {
	set_country_flag = SS_cavalry_division_early
	division_template = {
		name = "SS-Kav. Div."
		priority = 2
		division_names_group = SS_CAV_01
		is_locked = yes
		regiments = {
			SS_cavalry = { x = 0 y = 0 }
			SS_cavalry = { x = 0 y = 1 }
			SS_cavalry = { x = 0 y = 2 }
			
			SS_cavalry = { x = 1 y = 0 }
			SS_cavalry = { x = 1 y = 1 }
			SS_cavalry = { x = 1 y = 2 }
			
			SS_cavalry = { x = 2 y = 0 }
			SS_cavalry = { x = 2 y = 1 }
			SS_cavalry = { x = 2 y = 2 }

		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			artillery = { x = 0 y = 2 }
		}
	}
}
SS_cavalry_division_late = {
	set_country_flag = SS_cavalry_division_late
	division_template = {
		name = "SS-Kav. Div. (n.A.)"
		priority = 2
		division_names_group = SS_CAV_01
		is_locked = yes
		regiments = {
			SS_cavalry = { x = 0 y = 0 }
			SS_cavalry = { x = 0 y = 1 }
			SS_cavalry = { x = 0 y = 2 }
			
			SS_cavalry = { x = 1 y = 0 }
			SS_cavalry = { x = 1 y = 1 }
			SS_cavalry = { x = 1 y = 2 }
			
			SS_cavalry = { x = 2 y = 0 }
			SS_cavalry = { x = 2 y = 1 }
			SS_cavalry = { x = 2 y = 2 }

			SS_cavalry = { x = 3 y = 0 }
			SS_cavalry = { x = 3 y = 1 }
			SS_cavalry = { x = 3 y = 2 }
		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			field_hospital = { x = 0 y = 2 }
			signal_company = { x = 0 y = 3 }
			artillery = { x = 0 y = 4 }
		}
	}
}
SS_motorised_division_early = { #For 2. SS-Panzer-Division 'Das Reich'
	set_country_flag = SS_motorised_division_early
	division_template = {
		name = "SS-Gren. Div. (mot.)"
		priority = 2
		division_names_group = SS_MOT_01
		is_locked = yes
		regiments = {
			SS_motorized = { x = 0 y = 0 }
			SS_motorized = { x = 0 y = 1 }
			SS_motorized = { x = 0 y = 2 }

			SS_motorized = { x = 1 y = 0 }
			SS_motorized = { x = 1 y = 1 }
			SS_motorized = { x = 1 y = 2 }

			SS_mot_artillery_brigade = { x = 2 y = 0 }
			SS_mot_anti_tank_brigade = { x = 2 y = 1 }
		}
		support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			signal_company = { x = 0 y = 2 }
			military_police = { x = 0 y = 3 }
			anti_air = { x = 0 y = 4 }
		}
	}
}
SS_motorised_division_late = {
	set_country_flag = SS_motorised_division_late
	division_template = {
		name = "SS-Gren. Div. (mot.) (n.A.)"
		priority = 2
		division_names_group = SS_MOT_01
		is_locked = yes
		regiments = {
			SS_motorized = { x = 0 y = 0 }
			SS_motorized = { x = 0 y = 1 }
			SS_motorized = { x = 0 y = 2 }

			SS_motorized = { x = 1 y = 0 }
			SS_motorized = { x = 1 y = 1 }
			SS_motorized = { x = 1 y = 2 }

			SS_motorized = { x = 2 y = 0 }
			SS_motorized = { x = 2 y = 1 }
			SS_motorized = { x = 2 y = 2 }

			SS_mot_artillery_brigade = { x = 3 y = 0 } 
		
			SS_mot_anti_tank_brigade = { x = 4 y = 0 }
			SS_mot_anti_tank_brigade = { x = 4 y = 1 }
			SS_mot_anti_air_brigade = { x = 4 y = 2 }
		}
		support = {
			mot_recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			field_hospital = { x = 0 y = 2 }
			signal_company = { x = 0 y = 3 }
			artillery = { x = 0 y = 4 }
		}
	}
}
SS_mechanised_division_early = {
	set_country_flag = SS_mechanised_division_early
	division_template = {
		name = "SS-Pz.Gren. Div."
		priority = 2
		division_names_group = SS_MEC_01
		is_locked = yes
		regiments = {
			SS_mechanized = { x = 0 y = 0 }
			SS_mechanized = { x = 0 y = 1 }
			SS_mechanized = { x = 0 y = 2 }

			SS_mechanized = { x = 1 y = 0 }
			SS_mechanized = { x = 1 y = 1 }
			SS_mechanized = { x = 1 y = 2 }
			
			SS_mechanized = { x = 2 y = 0 }
			SS_mechanized = { x = 2 y = 1 }
			SS_mechanized = { x = 2 y = 2 }
		}
		support = {
			mot_recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			field_hospital = { x = 0 y = 2 }
			signal_company = { x = 0 y = 3 }
			artillery = { x = 0 y = 4 }
		}
	}
}
SS_mechanised_division_late = {
	set_country_flag = SS_mechanised_division_late
	division_template = {
		name = "SS-Pz.Gren. Div. (n.A.)"
		priority = 2
		division_names_group = SS_MEC_01
		is_locked = yes
		regiments = {
			SS_mechanized = { x = 0 y = 0 }
			SS_mechanized = { x = 0 y = 1 }
			SS_mechanized = { x = 0 y = 2 }
			
			SS_mechanized = { x = 1 y = 0 }
			SS_mechanized = { x = 1 y = 1 }
			SS_mechanized = { x = 1 y = 2 }
	
			SS_medium_armor = { x = 2 y = 0 }
			SS_medium_tank_destroyer_brigade = { x = 2 y = 1 }
			SS_medium_tank_destroyer_brigade = { x = 2 y = 2 }
			
			SS_mot_artillery_brigade = { x = 3 y = 0 } 
			
			SS_mot_anti_tank_brigade = { x = 4 y = 0 }
			SS_mot_anti_tank_brigade = { x = 4 y = 1 }
			SS_mot_anti_air_brigade = { x = 4 y = 2 }
		}
		support = {
			mot_recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			field_hospital = { x = 0 y = 2 }
			signal_company = { x = 0 y = 3 }
			artillery = { x = 0 y = 4 }
		}
	}
}
SS_armored_division_early = {
	set_country_flag = SS_armored_division_early
	division_template = {
		name = "SS-Panzer Div."
		priority = 2
		division_names_group = SS_ARM_01
		is_locked = yes
		regiments = {
			SS_medium_armor = { x = 0 y = 0 }
			SS_medium_armor = { x = 0 y = 1 }
			SS_medium_armor = { x = 0 y = 2 }
			SS_medium_armor = { x = 0 y = 3 }

			SS_medium_tank_destroyer_brigade = { x = 1 y = 0 }
			SS_medium_tank_destroyer_brigade = { x = 1 y = 1 }
			SS_medium_sp_artillery_brigade = { x = 1 y = 2 }

			SS_motorized = { x = 2 y = 0 }
			SS_motorized = { x = 2 y = 1 }
			SS_motorized = { x = 2 y = 2 }

			SS_motorized = { x = 3 y = 0 }
			SS_motorized = { x = 3 y = 1 }
			SS_motorized = { x = 3 y = 2 }

			SS_mot_artillery_brigade = { x = 4 y = 0 } 
			SS_mot_anti_air_brigade = { x = 4 y = 1 }
		}
		support = {
			mot_recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			field_hospital = { x = 0 y = 2 }
			signal_company = { x = 0 y = 3 }
			maintenance_company = { x = 0 y = 4 }
		}
	}
}
SS_armored_division_late = {
	set_country_flag = SS_armored_division_late
	division_template = {
		name = "SS-Panzer Div. (n.A.)"
		priority = 2
		division_names_group = SS_ARM_01
		is_locked = yes
		regiments = {
			SS_medium_armor = { x = 0 y = 0 }
			SS_medium_armor = { x = 0 y = 1 }
			SS_medium_armor = { x = 0 y = 2 }
			
			SS_heavy_armor = { x = 1 y = 0 }
			SS_medium_tank_destroyer_brigade = { x = 1 y = 1 }
			SS_medium_tank_destroyer_brigade = { x = 1 y = 2 }
			SS_medium_sp_artillery_brigade = { x = 1 y = 3 }
	
			SS_mechanized = { x = 2 y = 0 }
			SS_mechanized = { x = 2 y = 1 }
			SS_mechanized = { x = 2 y = 2 }
	
			SS_mechanized = { x = 3 y = 0 }
			SS_mechanized = { x = 3 y = 1 }
			SS_mechanized = { x = 3 y = 2 }
			
			SS_mot_rocket_artillery_brigade = { x = 4 y = 0 } 
			SS_mot_anti_air_brigade = { x = 4 y = 1 }
		}
		support = {
			mot_recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			field_hospital = { x = 0 y = 2 }
			signal_company = { x = 0 y = 3 }
			maintenance_company = { x = 0 y = 4 }
		}
	}
}

SS_resolve_effects_wehrmacht = {
	if = {
		limit = {
			AND = {
				check_variable = { var = wehrmacht_anger value = 0 compare = greater_than_or_equals }
				check_variable = { wehrmacht_anger < 10 }
			}
		}
		random_list = {
			50 = { country_event = { id = ss_recruitment_event.10 days = 1 } } #generals protest
			50 = {}
		}
	}
	if = {
		limit = {
			AND = {
				check_variable = { var = wehrmacht_anger value = 10 compare = greater_than_or_equals }
				check_variable = { wehrmacht_anger < 20 }
			}
		}
		random_list = {
			50 = { country_event = { id = ss_recruitment_event.10 days = 1 } } #generals protest
			25 = { country_event = { id = ss_recruitment_event.11 days = 1 } } #general resigns in protest
			25 = {}
		}
	}
	if = {
		limit = {
			AND = {
				check_variable = { var = wehrmacht_anger value = 20 compare = greater_than_or_equals }
				check_variable = { wehrmacht_anger < 30 }
			}
		}
		random_list = {
			50 = { country_event = { id = ss_recruitment_event.10 days = 1 } } #generals protest
			25 = { country_event = { id = ss_recruitment_event.11 days = 1 } } #general resigns in protest
			25 = { country_event = { id = ss_recruitment_event.12 days = 1 } } #generals plot
		}
	}
	if = {
		limit = {
			AND = {
				check_variable = { var = wehrmacht_anger value = 30 compare = greater_than_or_equals }
				check_variable = { wehrmacht_anger < 40 }
			}
		}
		random_list = {
			35 = { country_event = { id = ss_recruitment_event.10 days = 1 } } #generals protest
			35 = { country_event = { id = ss_recruitment_event.11 days = 1 } } #general resign
			30 = { country_event = { id = ss_recruitment_event.12 days = 1 } } #generals plot
		}
	}
	if = {
		limit = {
			AND = {
				check_variable = { var = wehrmacht_anger value = 40 compare = greater_than_or_equals }
				check_variable = { wehrmacht_anger < 50 }
			}
		}
		random_list = {
			35 = { country_event = { id = ss_recruitment_event.11 days = 1 } } #general resigns
			55 = { country_event = { id = ss_recruitment_event.12 days = 1 } } #generals plot
			10 = { 
				modifier = {
					NOT = {
						any_unit_leader = {
							has_unit_leader_flag = GER_SS_conspirator
						}
					}
					factor = 0
				}
				country_event = { id = ss_recruitment_event.13 days = 1 } 
			} #assassination attempt
		}
	}
	if = {
		limit = {
			AND = {
				check_variable = { var = wehrmacht_anger value = 50 compare = greater_than_or_equals }
				check_variable = { wehrmacht_anger < 60 }
			}
		}
		random_list = {
			75 = { country_event = { id = ss_recruitment_event.12 days = 1 } } #generals plot
			15 = { 
				modifier = {
					NOT = {
						any_unit_leader = {
							has_unit_leader_flag = GER_SS_conspirator
						}
					}
					factor = 0
				}
				country_event = { id = ss_recruitment_event.13 days = 1 } 
			} #assassination attempt
			10 = { country_event = { id = ss_recruitment_event.14 days = 1 } } #civil war
		}
	}
	if = {
		limit = {
			check_variable = { var = wehrmacht_anger value = 60 compare = greater_than_or_equals }
		}
		random_list = {
			15 = { 
				modifier = {
					NOT = {
						any_unit_leader = {
							has_unit_leader_flag = GER_SS_conspirator
						}
					}
					factor = 0
				}
				country_event = { id = ss_recruitment_event.13 days = 1 } 
			} #assassination attempt
			10 = { country_event = { id = ss_recruitment_event.14 days = 1 } } #civil war
		}
	}
}

SS_resolve_effects_ss = {
	if = {
		limit = {
			AND = {
				check_variable = { var = SS_anger value = 0 compare = greater_than_or_equals }
				check_variable = { SS_anger < 10 }
			}
		}
		random_list = {
			50 = { country_event = { id = ss_recruitment_event.20 days = 1 } } #Himmler protests
			50 = {}
		}
	}
	if = {
		limit = {
			AND = {
				check_variable = { var = SS_anger value = 10 compare = greater_than_or_equals }
				check_variable = { SS_anger < 20 }
			}
		}
		random_list = {
			50 = { country_event = { id = ss_recruitment_event.20 days = 1 } } #Himmler protest
			25 = { country_event = { id = ss_recruitment_event.21 days = 1 } } #secret meeting of SS officers
			25 = {}
		}
	}
	if = {
		limit = {
			AND = {
				check_variable = { var = SS_anger value = 20 compare = greater_than_or_equals }
				check_variable = { SS_anger < 30 }
			}
		}
		random_list = {
			50 = { country_event = { id = ss_recruitment_event.20 days = 1 } } #Himmler protest
			25 = { country_event = { id = ss_recruitment_event.21 days = 1 } } #SS officials meet
			25 = { country_event = { id = ss_recruitment_event.22 days = 1 } } #SS reroutes factory output
		}
	}
	if = {
		limit = {
			AND = {
				check_variable = { var = SS_anger value = 30 compare = greater_than_or_equals }
				check_variable = { SS_anger < 40 }
			}
		}
		random_list = {
			35 = { country_event = { id = ss_recruitment_event.21 days = 1 } } #SS officials meet
			35 = { country_event = { id = ss_recruitment_event.22 days = 1 } } #SS reroutes factory output
			30 = { country_event = { id = ss_recruitment_event.23 days = 1 } } #Himmler plots plot
		}
	}
	if = {
		limit = {
			AND = {
				check_variable = { var = SS_anger value = 40 compare = greater_than_or_equals }
				check_variable = { SS_anger < 50 }
			}
		}
		random_list = {
			35 = { country_event = { id = ss_recruitment_event.21 days = 1 } } #SS officials meet
			55 = { country_event = { id = ss_recruitment_event.23 days = 1 } } #Himmler plots
			10 = { country_event = { id = ss_recruitment_event.24 days = 1 } } #assassination attempt - SS
		}
	}
	if = {
		limit = {
			AND = {
				check_variable = { var = SS_anger value = 50 compare = greater_than_or_equals }
				check_variable = { SS_anger < 60 }
			}
		}
		random_list = {
			75 = { country_event = { id = ss_recruitment_event.23 days = 1 } } #Himmler plots
			15 = { country_event = { id = ss_recruitment_event.24 days = 1 } } #assassination attempt - SS
			10 = { country_event = { id = ss_recruitment_event.25 days = 1 } } #fascist on fascist violence
		}
	}
	if = {
		limit = {
			check_variable = { var = SS_anger value = 60 compare = greater_than_or_equals }
		}
		random_list = {
			15 = { country_event = { id = ss_recruitment_event.24 days = 1 } } #assassination attempt - SS
			10 = { country_event = { id = ss_recruitment_event.25 days = 1 } } #fascist on fascist violence
		}
	}
}