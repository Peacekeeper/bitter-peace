#	Example:
# 
#	example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}
#
#
#	In a script file:
#
#	effect = {
#		add_dynamic_modifier = {
#			modifier = example_dynamic_modifier
#			scope = GER # optional, if you specify this your dynamic modifier scoped to this scope (root is the effect scope)
#			days = 14 # optional, will be removed after this many days passes
#		}
#	}
#
#	can be added to countries, states or unit leaders
#	will only updated daily, unless forced by force_update_dynamic_modifier effect

#########################
#### ASIA MAFIA WARS ####
#########################

jap_triad_activity = {
	enable = { always = yes }

	icon = GFX_modifiers_jap_triad_activity
	
	resistance_growth = 0.03
	resistance_target = 0.1
	resistance_decay = -0.1
	state_resources_factor = -0.01
	weekly_manpower = -50
}

jap_yakuza_activity = {
	enable = { always = yes }

	icon = GFX_modifiers_jap_yakuza_activity
	
	resistance_growth = -0.03
	resistance_target = -0.1
	resistance_decay = 0.1
	state_resources_factor = 0.05
	weekly_manpower = -50
}

jap_loyal_triad = {
	enable = { always = yes }

	icon = GFX_modifiers_jap_loyal_triad
	
	resistance_growth = -0.01
	resistance_target = -0.1
	resistance_decay = 0.1
	state_resources_factor = 0.05
	weekly_manpower = -50
}

jap_doihara_business = {
	enable = { always = yes }

	icon = GFX_modifiers_jap_doihara_business
	
	resistance_growth = -0.01
	resistance_target = -0.1
	resistance_decay = 0.1
	state_resources_factor = -0.1
	weekly_manpower = -200
}

jap_triad_gb = {
	enable = { always = yes }

	icon = GFX_modifiers_jap_triad_gb
	
	resistance_growth = 0.03
	resistance_target = 0.1
	resistance_decay = -0.1
	state_resources_factor = -0.01
	weekly_manpower = -50
}