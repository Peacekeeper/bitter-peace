ideas = {
	
	political_advisor = {

		WLS_jeffrey_hamm = {
			
			picture = jeffrey_hamm
			
			allowed = {
				original_tag = WLS
			}
			
			traits = { fascist_demagogue }			
			ai_will_do = {
				factor = 1
			}
		}
		WLS_david_james_davies = {
			
			picture = political_advisor_europe_1
			
			allowed = {
				original_tag = WLS
			}
			
			traits = { captain_of_industry }			
			ai_will_do = {
				factor = 1
			}
		}
		WLS_lewis_valentine = {
			
			picture = lewis_valentine
			
			allowed = {
				original_tag = WLS
			}
			
			traits = { silent_workhorse }			
			ai_will_do = {
				factor = 1
			}
		}
		WLS_john_edward_daniel = {
			
			picture = political_advisor_europe_2
			
			allowed = {
				original_tag = WLS
			}
			
			traits = { smooth_talking_charmer }			
			ai_will_do = {
				factor = 1
			}
		}
		WLS_abi_williams = {
			
			picture = political_advisor_europe_3
			
			allowed = {
				original_tag = WLS
			}
			
			traits = { compassionate_gentleman }			
			ai_will_do = {
				factor = 1
			}
		}
		WLS_gwynfor_evans = {
			
			picture = gwynfor_evans
			
			allowed = {
				original_tag = WLS
			}
			
			traits = { popular_figurehead }			
			ai_will_do = {
				factor = 1
			}
		}		
		WLS_clayton_russon = {
			
			picture = clayton_russon
			
			allowed = {
				original_tag = WLS
			}
			
			traits = { war_industrialist }			
			ai_will_do = {
				factor = 1
			}
		}		
		WLS_herbert_merrett = {
			
			picture = political_advisor_europe_4
			
			allowed = {
				original_tag = WLS
			}
			
			traits = { armaments_organizer }			
			ai_will_do = {
				factor = 1
			}
		}		
	}
	army_chief = {

		WLS_tasker_watkins = {
			
			picture = tasker_watkins
			
			allowed = {
				original_tag = WLS
			}
			
			traits = { army_chief_organizational_1 }			
			ai_will_do = {
				factor = 1
			}
		}
	}
	navy_chief = {

		WLS_john_linton = {
			
			picture = political_advisor_europe_3
			
			allowed = {
				original_tag = WLS
			}
			
			traits = { navy_chief_commerce_raiding_2 }			
			ai_will_do = {
				factor = 1
			}
		}
	}
	air_chief = {

		WLS_raymon_hughes = {
			
			picture = political_advisor_europe_2
			
			allowed = {
				original_tag = WLS
			}
			
			traits = { air_air_superiority_1 }			
			ai_will_do = {
				factor = 1
			}
		}
	}
	high_command = {

		WLS_leigh_vaughan_-_henry = {
			ledger = army

			picture = political_advisor_europe_1
			
			allowed = {
				original_tag = WLS
			}
			
			traits = { army_regrouping_1 }			
			ai_will_do = {
				factor = 1
			}
		}
	}
}	