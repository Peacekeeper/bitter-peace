ideas = {
	country = {
		FRA_war_reparations = {
			allowed = { always = no }
			available = {
			}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = SPR_seize_the_gold_reserves
			modifier = {
				cic_to_overlord_factor = 0.50
				mic_to_overlord_factor = 0.25
			}
		}

		FRA_the_third_treaty_of_versailles = {
			allowed = { always = no }
			available = {
			}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = SPR_disbanded_army
			modifier = {
				army_org_factor = -0.05
				conscription_factor = -0.35
				mobilization_laws_cost_factor = 0.25
				production_speed_arms_factory_factor = -0.3
				conversion_cost_civ_to_mil_factor = 0.3
				production_speed_dockyard_factor = -0.3
			}
		}
	}
}