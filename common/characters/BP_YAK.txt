characters={
	YAK_platon_oyunsky = {
		name = YAK_platon_oyunsky
		portraits = {
			civilian = {
				large = "gfx/leaders/YAK/Portrait_Yakutia_Platon_Oyunsky.dds"
			}
		}
		country_leader = {
			ideology = fascism_ideology
			expire = "1965.1.1"
			id = -1
		}
	}
	YAK_vyacheslav_shtyrov = {
		name = YAK_vyacheslav_shtyrov
		portraits = {
			civilian = {
				large = "gfx/leaders/RAJ/Portrait_The_Raj_Political_Leader_Generic_2.dds"
			}
		}
		country_leader = {
			ideology = conservatism
			expire = "1960.1.1.1"
			id = -1
		}
	}
}
