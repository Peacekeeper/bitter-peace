###############################################################################################################
# Bitter Peace Soviet Decisions ###############################################################################
###############################################################################################################

###############################################################################################################
# Great Patriotic War #########################################################################################
###############################################################################################################
SOV_great_patriotic_war = {
	#### The Sacred War ####
	# SOV_sacred_war = {

	# 	icon = generic_army_support

	# 	fire_only_once = yes
	# 	cost = 0	
	
	# 	allowed = {
	# 		tag = SOV
	# 	}
	
	# 	available = {
	# 		has_war = yes
	# 		has_civil_war = no
	# 	}
		
	# 	visible = {
	# 		any_enemy_country = {
	# 			has_government = fascism
	# 			controls_state = 205
	# 			controls_state = 242
	# 			controls_state = 243
	# 		}
	# 	}	
		
	# 	ai_will_do = {
	# 		factor = 10
	# 		modifier = {
	# 			surrender_progress > 0.25
	# 			factor = 100
	# 		}
	# 	}
		
	# 	complete_effect = {
	# 		add_ideas = SOV_sacred_war
	# 		remove_ideas = SOV_army_destroy	
	# 	}	
	# }
	#### Destroy DneproGES ####
	SOV_destroy_dnepr_dam = {

		icon = hol_inundate_water_lines

		fire_only_once = yes
		cost = 0

		allowed = {
			tag = SOV
		}

		available = {
			has_war = yes
			controls_state = 226 #Dnepropetrovsk
		}
		
		visible = {
			has_civil_war = no
		}

		days_remove = 3

		highlight_states = {
			highlight_state_targets = { state = 226}
			highlight_provinces = {
				11437
				11422
				453
				9419
				11405
				588
				3573
				11715
				737
			}
		}
		
		ai_will_do = {
			base = 0
			modifier = {
				add = 1
				any_enemy_country = {
					controls_state = 197 #Mykolaiv
					controls_state = 203 #Cherkasy
					controls_state = 259 #Poltava
					controls_state = 221 #Kharkov
					controls_state = 227 #Stalino
				}
			}
		}
		
		complete_effect = {
			set_global_flag = destruction_of_dneprostroi_dam
			226 = { #Dnepropetrovsk
				add_province_modifier = {
					static_modifiers = { flooded }
					province = {
						id = 11437
						id = 11422
						id = 453
						id = 9419
					}
				}
				damage_building = {
					type = infrastructure
					damage = 2
				}
				damage_building = {
					type = industrial_complex
					damage = 2
				}
			}
			200 = {	#Zaporozhe
				add_province_modifier = {
					static_modifiers = { flooded }
					province = {
						id = 11405
						id = 588
					}
				}
				damage_building = {
					type = infrastructure
					damage = 2
				}
			}
			196 = {	#Kherson
				add_province_modifier = {
					static_modifiers = { flooded }
					province = {
						id = 3573
						id = 11715
						id = 737
					}
				}
				damage_building = {
					type = infrastructure
					damage = 2
				}
			}
			hidden_effect = { 
#				GER = { country_event = BP_RKU.6 } 
#				RKU = { country_event = BP_RKU.6 } 
			}
		}	
	}
	#### Sign a Peace Treaty with Germany ####
	SOV_sign_a_peace_treaty_with_germany = {

		icon = generic_nationalism

		fire_only_once = yes
		cost = 0
	
		allowed = {
			tag = SOV
		}
	
		available = {
			OR = {
				surrender_progress > 0.8
				AND = {
					NOT = {
						controls_state = 195 #Leningrad
						controls_state = 219 #Moscow
						controls_state = 217 #Stalingrad
						controls_state = 229 #Baku
					}
				}
			}
		}
		
		visible = {
			has_war = yes
			any_enemy_country = {
				tag = GER 
				has_government = fascism
			}
			has_civil_war = no
		}
		
		ai_will_do = {
			factor = 10
			modifier = {
				surrender_progress > 0.80
				factor = 100
			}
		}
		
		complete_effect = {
			GER = { country_event = BP_USSR.9 }
		}
	}
}

###############################################################################################################
# Operations ##################################################################################################
###############################################################################################################

operations = {
	#### Operation Countenance ####
	SOV_seize_iran = {

		icon = generic_operation

		fire_only_once = yes	
		cost = 25
		
		allowed = {
			tag = SOV
		}

		available = {		
			date > 1941.9.10		
			PER = {
				exists = yes			
				is_puppet = no
				is_in_faction = no		
				NOT = { has_war_with = SOV }
			}
		}

		visible = {
			has_war_with = GER
			ENG = { has_war_with = GER }
		}

		ai_will_do = {
			factor = 10
		}
		
		complete_effect = {
			ENG = { country_event = { days = 1 id = BP_england.12 } }
		}
	}
}
###############################################################################################################
# Special Projects ############################################################################################
###############################################################################################################

special_projects = {
	#### Restoration of Dnieper Dam ####
	SOV_drain_the_water_dnepr_dam = {

		icon = hol_drain_water_lines

		allowed = {
			tag = SOV
		}

		available = {
			has_full_control_of_state = 226
			has_full_control_of_state = 200
			has_full_control_of_state = 196
		}

		cost = 25

		fire_only_once = no

		days_remove = 80

		highlight_states = {
			highlight_provinces = {
				11437
				11422
				453
				9419
				11405
				588
				3573
				11715
				737
			}
		}

		modifier = {
			civilian_factory_use = 15
		}

		visible = {
			has_global_flag = destruction_of_dneprostroi_dam
		}

		ai_will_do = {
			base = 0
			modifier = {
				add = 1
				NOT = {
					any_enemy_country = {
						controls_state = 197 #Mykolaiv
						controls_state = 203 #Cherkasy
						controls_state = 259 #Poltava
						controls_state = 221 #Kharkov
						controls_state = 227 #Stalino
					}
				}
			}
		}

		remove_effect = {
			clr_global_flag = destruction_of_dneprostroi_dam
			226 = { #Dnepropetrovsk
				remove_province_modifier = {
					static_modifiers = { flooded }
					province = {
						id = 11437
						id = 11422
						id = 453
						id = 9419
					}
				}
			}
			200 = {	#Zaporozhe
				remove_province_modifier = {
					static_modifiers = { flooded }
					province = {
						id = 11405
						id = 588
					}
				}
			}
			196 = {	#Kherson
				remove_province_modifier = {
					static_modifiers = { flooded }
					province = {
						id = 3573
						id = 11715
						id = 737
					}
				}
			}
		}
	}
}

###############################################################################################################
# Turkestan  ############################################################################################
###############################################################################################################

BP_turkestan_trouble = {

	SOV_create_warlord = {

		priority = 10

		icon = generic_ignite_civil_war
		fire_only_once = yes
		
		cost = 50
		
		available = {
		tag = SOV
		}

		visible = {
		tag = SOV
		}

		ai_will_do = {
			factor = 100
		}

		complete_effect = {
			hidden_effect = {
				create_dynamic_country = {
					original_tag = KAZ
					set_cosmetic_tag = TSSR
					transfer_state = 588
					add_state_core = 584
					add_state_core = 832
					add_state_core = 831
					add_state_core = 823
					add_state_core = 830
					add_state_core = 585
					add_state_core = 405
					add_state_core = 742
					add_state_core = 732
					
					set_rule = {
						can_join_factions = no
					}
					
					588 = { create_unit = { division = "name = \"Strelkovaya Diviziya\" division_template = \"Strelkovaya Diviziya\" start_experience_factor = 0.5 start_equipment_factor = 0.5" owner = SST } }

					add_ideas = {
						limited_exports
						limited_conscription
						partial_economic_mobilisation
					}
					
					set_politics = {
						ruling_party = communism
						elections_allowed = no
					}
					set_popularities = {
						democratic = 10
						fascism = 10
						communism = 70
						neutrality = 10
					}

					create_country_leader = {
						name = "Nikolay Skvortsov"
						desc = ""
						picture = "gfx/leaders/KAZ/portrait_kaz_nikolay_skvortsov.dds"
						expire = "1953.3.1"
						ideology = stalinism
						traits = {
						}
					}
				}
			}
		}
	}
	
	SOV_military_help = {

		icon = generic_operation
		
		cost = 50

		available = {
			tag = SOV
			SST = {
				exists = yes
			}
		}

		visible = {
			tag = SOV
			SST = {
				exists = yes
			}
		}

		ai_will_do = {
			factor = 50
		}
		
		complete_effect = {
			SST = { 588 = { create_unit = { division = "name = \"Strelkovaya Diviziya\" division_template = \"Strelkovaya Diviziya\" start_experience_factor = 0.5 start_equipment_factor = 0.5" owner = SST } } }
		}
	}

	SOV_crush_warlords = {

		icon = generic_operation
		
		cost = 50

		available = {
			SST = {
				exists = yes
				has_war = no
			}
		}

		visible = {
			tag = SOV
			country_exists = SST
			OR = {
				country_exists = URA
				country_exists = ALD
				country_exists = YUL
				country_exists = GYR
				country_exists = AKM
				country_exists = TMS
				country_exists = KYR
				country_exists = TAJ
				country_exists = UZB
				country_exists = TKL
				country_exists = YJD
				country_exists = KKP
				country_exists = BUK
			}
		}

		ai_will_do = {
			factor = 50
		}
		
		complete_effect = {
			SST = {
				random_neighbor_country = {
					limit = {
						OR = {
							tag = URA
							tag = ALD
							tag = YUL
							tag = GYR
							tag = AKM
							tag = UST
							tag = TMS
							tag = TAJ
							tag = KYR
							tag = UZB
							tag = TKL
							tag = YJD
							tag = KKP
							tag = BUK
						}
					}
					SST = {
						declare_war_on = {
							target = PREV
							type = annex_everything
						}
					}
				}
			}
		}
	}

	SOV_finish_turkestan = {

		icon = generic_operation
		fire_only_once = yes
		cost = 10

		available = {
			SST = {
				exists = yes
				has_war = no
				num_of_controlled_states > 14
			}
		}

		visible = {
			tag = SOV
			country_exists = SST
		}

		ai_will_do = {
			factor = 50
		}
		
		complete_effect = {
			SST = { country_event = { days = 1 id = BP_USSR.185 } }
		}
	}

	SOV_basmachi = {

		icon = generic_operation
		
		available = {
			hidden_trigger = {
				always = no
			}
		}

		visible = {
			OR = {
				tag = SOV
				tag = GER
				tag = JAP
			}
		}

		days_mission_timeout = 90

		fire_only_once = yes

		activation = {
			has_global_flag = turkestan_collapse_end
		}
		
		cancel_trigger = {

		}

		is_good = no

		timeout_effect = {
			hidden_effect = {

				if = {
					limit = {
						tag = SOV  
					}

				create_dynamic_country = {
					original_tag = KAZ
					set_cosmetic_tag = KAZ_turkestan_revolt
					transfer_state = 405

					set_rule = {
						can_join_factions = no
					}
					
					load_oob = "KAZ_turkestan_riot_new"

					add_ideas = {
						limited_exports
						limited_conscription
						partial_economic_mobilisation
					}
				
					set_politics = {
						ruling_party = neutrality
						elections_allowed = no
					}
					set_popularities = {
						democratic = 10
						fascism = 20
						communism = 10
						neutrality = 60
					}

					create_country_leader = {
						name = "Veli Kayyum Han"
						desc = "POLITICS_VELI_KAYYUM_HAN_DESC"
						picture = "gfx/leaders/KAZ/Portrait_Kazahstan_Veli_Kayyum_Han.dds"
						expire = "1965.1.1"
						ideology = despotism 
						traits = {
						}
					}
				}
				TRR = {
					every_country = {
						limit = {
							OR = {
								tag = URA
								tag = ALD
								tag = YUL
								tag = GYR
								tag = AKM
								tag = UST
								tag = TMS
								tag = TAJ
								tag = KYR
								tag = UZB
								tag = SST
								tag = TKL
								tag = SST
								tag = YJD
								tag = KKP
								tag = BUK
							}
						}
					TRR = {
						declare_war_on = {
							target = PREV
							type = annex_everything
							}
						}
					}
				}
				}
			}
		}
	}
}
