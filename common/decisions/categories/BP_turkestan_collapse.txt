#########################################
#### BITTER PEACE TURKESTAN COLLAPSE ####
#########################################

BP_turkestan_trouble = {

	icon = GFX_decision_category_generic_crisis
	picture = GFX_decision_cat_picture_government_in_exile
	visible_when_empty = no
	priority = 80

	allowed = {
	OR = {
		tag = SOV
		tag = GER
		tag = JAP
		}
	}

	visible = {
		has_global_flag = turkestan_collapse_end
		
	}
}